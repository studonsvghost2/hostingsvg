# Hosting SVG

Repositoy zum hosten von SVGs für E-Tests auf StudOn.

## Diagramm Übersicht

*Anmerkung*: Alle älteren Diagramme (*a[1-6].svg*, *er[1-6].svg*) sind noch mit dem [alten Repository](https://gitlab.com/studonsvghost/hostingsvg) verlinkt.

Diagramm(e) | E-Test(s)
--- | ---
*2e1r chen/** | E/R Beziehungsinstanzen - Anzahl an Entites - Ober/Untergrenzen - 2E1R/Chen
*2e2r chen/** | E/R Beziehungsinstanzen - Anzahl an Entitäten - 2E2R/Chen <br/>E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 2E2R/Chen
*4e1i1c/** | EE/R - Anzahl an Entities - Ober/Untergrenzen - 4E1I1C
*4e1r chen/** | E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 4E1R/Chen
*6e4r chen/** | E/R 6E4R Chen - FAs bestimmen<br/>E/R 6E4R Chen - FAs bestimmen mithilfe von Armstrong-Axiomen
*eer equality/** | EE/R Äquivalenz (1)
*er-eer syntax/** | E/R - Fehlerhafte Diagramme (1) <br/>E/R - Fehlerhafte Diagramme (2) <br/>EE/R - Fehlerhafte Diagramme (1) <br/>EE/R - Fehlerhafte Diagramme (2)
*2e2r min-max.svg* | E/R Beziehungsinstanzen - Anzahl an Entitäten - 2E2R/MinMax
*4e3r chen.svg* | Anzahl an Entities - Ober/Untergrenzen - 4E3R Chen
*a1.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R/Chen
*a2.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R2T/Chen
*a3.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R2T/Chen - 2
*a4.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R3T/Chen
*a5.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R4T/Chen
*a6.svg* | E/R Beziehungsinstanzen - Anzahl an Entities - 2E2R1T/Chen
*er1.svg* | E/R Beziehungsinstanzen - Anzahl an Entitäten - 2E1R/MinMax <br/>E/R Beziehungsinstanzen - Anzahl an Entitäten - 2E1R/MinMax - 2 <br/>E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 2E1R/MinMax <br/>E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 2E1R/MinMax - a != b und c != d
*er2.svg* | E/R Beziehungsinstanzen - Anzahl an Entitäten - 3E1R/MinMax <br/>E/R Beziehungsinstanzen - Anzahl an Entitäten - 3E1R/MinMax - Große Zahlen <br/>E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 3E1R/MinMax - Extensionen
*er3.svg* | E/R Beziehungsinstanzen - Anzahl an Entitäten - 4E1R/MinMax - Große Zahlen <br/>E/R Beziehungsinstanzen - Anzahl an Entitäten - 4E1R/MinMax
*er4.svg* | E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 3E1R/Chen - Extensionen
*er5.svg* | [...]
*er6.svg* | E/R Beziehungsinstanzen - Anzahl an Beziehungsinstanzen - 3E1R1T/Chen - Extensionen
*mapping.svg* | Mapping E/R-Modell - Relationenmodell alle Tabellen <br/>Mapping E/R-Modell - Relationenmodell Entity A <br/>Mapping E/R-Modell - Relationenmodell Entity C <br/>Mapping E/R-Modell - Relationenmodell Entity W Primärschlüssel <br/>Mapping E/R-Modell - Relationenmodell Relation S <br/>Mapping E/R-Modell - Relationenmodell Relation T <br/>Mapping E/R-Modell - Relationenmodell schwache Entity W
*mapping2.svg* | Mapping 2 - Anzahl an Tabellen<br/>Mapping 2 - Anzahl an Attributen - Tabelle [*A \| B \| C \| D \| E \| R \| Q \| C2*]<br/>Mapping 2 - Primärschlüssel: Anzahl an Attributen - Tabelle [*B \| D \| E \| R \| C2*]<br/>Mapping 2 - Fremdschlüssel Optionen - Tabelle [D \| E \| Q \| C2]
*weak entity.svg* | E/R Beziehungsinstanzen - Anzahl an Entitäten - Schwacher Entity Typ
